<?php

class CommentTableSeeder extends Seeder {

    public function run()
    {
        DB::table('comments')->delete();

        Comment::create(array('id' => '1',
            'post_id' => '1',
            'message' => 'Hey, how are you?',
            'user_id' => '1'));

        Comment::create(array('id' => '3',
            'post_id' => '2',
            'message' => 'I have a law degree in facebook',
            'user_id' => '3'));

        Comment::create(array('id' => '4',
            'post_id' => '2',
            'message' => 'What..',
            'user_id' => '2'));

        Comment::create(array('id' => '5',
            'post_id' => '3',
            'message' => 'Kim and Kanye are the BERRRST',
            'user_id' => '2'));

        Comment::create(array('id' => '6',
            'post_id' => '6',
            'message' => 'I like turtles',
            'user_id' => '2'));

        Comment::create(array('id' => '7',
            'post_id' => '6',
            'message' => 'I like turtles',
            'user_id' => '2'));

        Comment::create(array('id' => '8',
            'post_id' => '6',
            'message' => 'I like turtles',
            'user_id' => '2'));

        Comment::create(array('id' => '9',
            'post_id' => '6',
            'message' => 'Kim and Kanye are the BERRRST',
            'user_id' => '2'));

        Comment::create(array('id' => '10',
            'post_id' => '6',
            'message' => 'Kim and Kanye are the BERRRST',
            'user_id' => '2'));

        Comment::create(array('id' => '11',
            'post_id' => '6',
            'message' => 'Kim and Kanye are the BERRRST',
            'user_id' => '2'));

        Comment::create(array('id' => '12',
            'post_id' => '6',
            'message' => 'Kim and Kanye are the BERRRST',
            'user_id' => '2'));

        Comment::create(array('id' => '13',
            'post_id' => '6',
            'message' => 'Kim and Kanye are the BERRRST',
            'user_id' => '2'));

        Comment::create(array('id' => '14',
            'post_id' => '6',
            'message' => 'Kim and Kanye are the BERRRST',
            'user_id' => '2'));

        Comment::create(array('id' => '15',
            'post_id' => '6',
            'message' => 'Kim and Kanye are the BERRRST',
            'user_id' => '2'));
    }

}