<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array('id' => '1',
            'email' => 'Bob@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'Bob',
            'DOB' => '1984-02-19',
        ));

        User::create(array('id' => '2',
            'email' => 'John@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'John',
            'DOB' => '1946-10-16',
        ));

        User::create(array('id' => '3',
            'email' => 'Tom@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'Tom',
            'DOB' => '1969-07-24',
        ));

        User::create(array('id' => '4',
            'email' => 'Alan@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'Alan Schiffler',
            'DOB' => '1990-03-30',
        ));
        User::create(array('id' => '5',
            'email' => 'Ella@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'Ariella Moser',
            'DOB' => '1994-07-24',
        ));
        User::create(array('id' => '6',
            'email' => 'Moose@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'Moose Sir',
            'DOB' => '2009-07-24',
        ));
        User::create(array('id' => '7',
            'email' => 'Joey@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'Joey Crazydog',
            'DOB' => '2011-07-24',
        ));
        User::create(array('id' => '8',
            'email' => 'Keith@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'Keith McLame',
            'DOB' => '1989-07-24',
        ));
        User::create(array('id' => '9',
            'email' => 'Tucsi@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'Tucsi Spoiled',
            'DOB' => '2007-07-24',
        ));
        User::create(array('id' => '10',
            'email' => 'Malteser@a.org',
            'password' => Hash::make(1234),
            'fullName' => 'I like maltesers man',
            'DOB' => '1800-07-24',
        ));
    }

}