<?php

class FriendshipTableSeeder extends Seeder {

    public function run()
    {
        DB::table('friendships')->delete();

        // John is friends with Bob
        Friendship::create(array('id' => '1',
            'userId_1' => '2',
            'userId_2' => '1'));
    }

}