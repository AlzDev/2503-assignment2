<?php

class PostTableSeeder extends Seeder {

    public function run()
    {
        DB::table('posts')->delete();

        Post::create(array('id' => '1',
            'title' => 'Bob\'s Post 1',
            'message' => 'Bob\'s public post',
            'user_id' => '1',
            'privacy' => 'Public'));

        Post::create(array('id' => '2',
            'title' => 'Bob\'s Post 2',
            'message' => 'Bob\'s friends post',
            'user_id' => '1',
            'privacy' => 'Friends'));

        Post::create(array('id' => '3',
            'title' => 'Bob\'s Post 3',
            'message' => 'Bob\'s private post',
            'user_id' => '1',
            'privacy' => 'Private'));
        
        Post::create(array('id' => '4',
            'title' => 'John\'s Post 1',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));

        Post::create(array('id' => '5',
            'title' => 'John\'s Post 2',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));

        Post::create(array('id' => '6',
            'title' => 'John\'s Post 3',
            'message' => 'Bob\'s private post',
            'user_id' => '2',
            'privacy' => 'Public'));

        Post::create(array('id' => '7',
            'title' => 'John\'s Post 4',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));

        Post::create(array('id' => '8',
            'title' => 'John\'s Post 5',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));

        Post::create(array('id' => '9',
            'title' => 'John\'s Post 6',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));

        Post::create(array('id' => '10',
            'title' => 'John\'s Post 7',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));

        Post::create(array('id' => '11',
            'title' => 'John\'s Post 8',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));

        Post::create(array('id' => '12',
            'title' => 'John\'s Post 9',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));
        
        Post::create(array('id' => '13',
            'title' => 'John\'s Post 10',
            'message' => 'I Like to spam!!!',
            'user_id' => '2',
            'privacy' => 'Public'));
    }

}