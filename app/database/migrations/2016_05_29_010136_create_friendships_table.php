<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendshipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('friendships', function ($table) {
            $table->increments('id');
            $table->integer('userId_1'); // Foreign key on USER table
            $table->integer('userId_2'); // Foreign key on USER table

            $table->timestamps();

            $table->foreign('userId_1')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('userId_2')->references('id')->on('users')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('friendships');
	}

}
