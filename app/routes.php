<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Misc. Routes
Route::get('/', 'PostController@index');
Route::post('search', 'UserController@search');
Route::get('erd', function(){
    return View::make('erd');
});
Route::get('docs', function(){
    return View::make('docs');
});

// User Routes
Route::resource('user', 'UserController');
Route::get('register', 'UserController@create');
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('logout', 'UserController@destroy');

// Friend Routes
Route::resource('friendship', 'FriendshipController');
Route::get('unfollow/{id}', 'FriendshipController@destroy');

// Post Routes
Route::resource('post', 'PostController');
Route::get('post/delete/{id}', 'PostController@destroy');

// Comment Routes
Route::resource('comment', 'CommentController');
Route::get('comment={id}/delete', 'CommentController@destroyComment');