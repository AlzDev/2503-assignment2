<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Carbon\Carbon;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

    use EloquentTrait, UserTrait, RemindableTrait;

    // DATABASE TABLE -------------------------------------------------------
    // define which table is used by this model.
    // @var string $table
    protected $table = 'users';
    protected $dates = ['DOB', 'created_at', 'updated_at'];


    public function age()
    {
        return $this->attributes['DOB']->diffInYears(Carbon::now());
    }

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 4 attributes able to be filled. Guarded is the
    // inverse of fillable, so these variables will be protected from mass
    // assignment.
    protected $fillable = array('fullName', 'DOB');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    public function posts() {
        return $this->hasMany('Post');
    }

    function friends() {
        return $this->belongsToMany('User', 'friendships', 'userId_1', 'userId_2');
    }

    function friendsWith() {
        return $this->belongsToMany('User', 'friendships', 'userId_2', 'userId_1');
    }

    public function isFriend($friendId) {
        return (boolean) $this->friends()->where('users.id', $friendId)->count();
    }

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('avatar', [
            'styles' => [
                'large' => '515x342',
                'medium' => '300x300',
                'thumb' => '52x52',
                'comment' => '32x32'
            ]
        ]);

        parent::__construct($attributes);
    }
}



