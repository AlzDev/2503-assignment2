<?php

class Comment extends Eloquent {

    protected $table = 'comments';
    protected $fillable = ['message', 'user_id', 'post_id'];
    protected $dates = ['created_at', 'updated_at'];

    // DEFINE RELATIONSHIPS --------------------------------------------------

    public function user()
    {
        return $this->belongsTo('User');
    }
    
    public function post()
    {
        return $this->belongsTo('Post');
    }
}