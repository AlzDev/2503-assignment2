<?php

class Post extends Eloquent
{
    // DATABASE TABLE -------------------------------------------------------
    // define which table is used by this model.
    // @var string $table
    protected $table = 'posts';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 5 attributes able to be filled
    protected $fillable = ['title', 'message', 'user_id', 'privacy'];
    protected $dates = ['created_at', 'updated_at'];

    // DEFINE RELATIONSHIPS --------------------------------------------------

    public function user(){
        return $this->belongsTo('User');
    }

    // each post HAS MANY comments
    public function comments()
    {
        return $this->hasMany('Comment');
    }
}