<?php

class FriendshipController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$User = Auth::user();
		$RequestedUser = User::find(Input::get('friend_id'));

		$friendship = new Friendship;
		$friendship->userId_1 = $User->id;
		$friendship->userId_2 = $RequestedUser->id;
		$friendship->save();

		return Redirect::back()->withFlashMessage('You and '. $RequestedUser->fullName .' are now friends. Good on you.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$friends = User::find(Auth::user()->id)->friends;
		$friendsWith = User::find(Auth::user()->id)->friendsWith;

		$result = $friends->merge($friendsWith);

		return View::make('user.list')->with('users', $result);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$friendship = Friendship::where('userId_1', '=', Auth::user()->id)
			->where('userId_2', '=', $id)
			->orWhere('userId_2', '=', Auth::user()->id)
			->where('userId_1', '=', $id)->first();
		$friendship->delete();

		return Redirect::back()->withFlashMessage('You are no longer friends with '. User::find($id)->fullName .'.');
	}


}
