<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $posts = Post::orderBy('created_at', 'desc')->get();
        $comments = Comment::all();
        
		return View::make('home')->with('posts', $posts)->with('comments', $comments);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('user.register');
	}

    public function login(){
		$userdata = array(
			'email' => Input::get('email'),
			'password' => Input::get('password'));
			
			//auth
			if (Auth::attempt($userdata)){
				return Redirect::to(URL::previous());
			} else {
				$errors = 'Login Error';
				return Redirect::to(URL::previous())->withInput()->withErrors($errors);
			}
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();
        $password = $input['password'];
        $encrypted = Hash::make($password);

        $rules = array(
            'email' => 'required|unique:users',
            'password' => 'required',
            'fullName' => 'required',
            'DOB' => 'required|date|date_format:Y-m-d'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            Input::flash();

            return Redirect::back()->withErrors($validator);
        } else {

            $user = new User;
            $user->email = $input['email'];
            $user->password = $encrypted;
            $user->fullName = $input['fullName'];
            $user->DOB = $input['DOB'];
			$user->avatar = $input['avatar'];
            $user->save();

            return Redirect::to('/')->withFlashMessage('User Created.');
        }
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		$posts = Post::where('user_id', '=', $user->id)->get();

		$comments = Comment::all();

		$friends = User::find($user->id)->friends;
		$friendsWith = User::find($user->id)->friendsWith;
		$friendCount = count($friends->merge($friendsWith));

		return View::make('user.profile')
			->with('user', $user)
			->with('posts', $posts)
			->with('comments', $comments)
			->with('friendCount', $friendCount);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$rules = array(
			'fullName' => 'required',
			'DOB' => 'required|date|date_format:Y-m-d'
		);

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			$messages = $validator->messages();

			Input::flash();
			return Redirect::back()->withErrors($validator);
		} else {
			$user = User::find($id);
			$user->fullName = $input['fullName'];
			$user->DOB = $input['DOB'];
			$user->avatar = $input['avatar'];
			$user->save();

			// redirect ----------------------------------------
			// redirect our user back to the index
			return Redirect::back()->withFlashMessage('Updated details.');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return Redirect::back()->withFlashMessage('Logged Out.');
	
	}


	public function search() {
		$searchText = Input::get('search');

		$users = User::where('email', 'LIKE', '%'.$searchText.'%')
			->orWhere('fullName', 'LIKE', '%'.$searchText.'%')->get();

		return View::make('user.list')
			->with('users', $users);
	}
	


}
