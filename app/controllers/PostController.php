<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = Post::orderBy('created_at', 'desc')->get();
        $comments = Comment::all();

        return View::make('home')
            ->with('posts', $posts)
            ->with('comments', $comments);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = array(
            'title'     =>  'required',
            'message'   =>  'required',
            'privacy'   =>  'required'
        );

        // do the validation
        // validate against the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {
            // get the error messages from the validator
            $messages = $validator->messages();

            // refill the form with previous values
            Input::flash();

            // redirect user back to home with the validator errors
            return Redirect::back()
                ->withErrors($validator);
        } else {
            // validation successful ---------------------------
            // submit the post to the database
            Post::create(array(
                'title'    =>  Input::get('title'),
                'message'  =>  Input::get('message'),
                'user_id'  =>  Auth::user()->id,
                'privacy'  => Input::get('privacy')
            ));

            // redirect ----------------------------------------
            // redirect our user back to home so they can do it all over again
            return Redirect::to('/');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $post = Post::find($id);
        $comments = Post::find($id)->comments;

        return View::make('posts.post_detail')
            ->with('post', $post)
            ->with('comments', $comments);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);
		$comments = Post::find($id)->comments;
		return View::make('edit')
			->with('post', $post)
			->with('comments', $comments);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($post_id)
	{
		$rules = array(
			'title'     =>  'required',
			'message'   =>  'required',
			'privacy'   =>  'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			$messages = $validator->messages();
			Input::flash();
			return Redirect::back()->withErrors($validator);
		} else {
			$post           = Post::find($post_id);
			$post->title    = Input::get('title');
			$post->message  = Input::get('message');
			$post->privacy  = Input::get('privacy');
			$post->save();

			return Redirect::back()->withFlashMessage('Updated Post.');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $comments = Comment::where('post_id','=', $id)->get();
        foreach ($comments as $comment) {
            $comment->delete();
        }
        $post = Post::find($id);
        $post->delete();

		return Redirect::to('/');
	}
	


}
