{{ Form::model($post, array('route' => array('post.update', $post->id), 'method' => 'PUT'))  }}

    <div class="form-group">
        {{ Form::text('title', '', array('placeholder'=>'Post title','class' => 'form-control', 'value' => $post->title)) }}
    </div>
    <div class="form-group">
        {{ Form::textarea('message', '', array('placeholder'=>'Post a story','class' => 'form-control', 'rows' => '4', 'value' => $post->message)) }}
    </div>
    <div>
        {{ Form::select('privacy', array('Public' => 'Public', 'Friends' => 'Friends', 'Private' => 'Private'), $post->privacy); }}
         User: <strong>{{{ Auth::user()->fullName }}}</strong>
        {{ Form::submit('Post status', array('class' => 'btn btn-info pull-right m-t-n-xs', 'style' => 'font-weight:600')) }}
    </div>

{{ Form::close() }}