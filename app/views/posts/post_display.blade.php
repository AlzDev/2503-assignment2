<br/>
<div class="panel panel-info">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <a href="#">
            <img alt="avatar" width="40" height="40" src="{{{ User::find($post->user_id)->avatar->url() }}}"  class="img-circle">
        </a>
        <strong>{{{ $post->title }}}</strong> by {{{ User::find($post->user_id)->fullName }}}

        <span class="pull-right">
            <a href="{{ URL::route('post.show', array($post->id)) }}"> View</a>
            @if (Auth::check())
                @if(Auth::user()->id == $post->user_id)
                <a href="{{ URL::route('post.edit', array($post->id)) }}"> | Edit</a>
                <a href="{{ URL::to('post/delete/' . $post->id) }}"> | Delete</a>
                @endif
            @endif
        </span>
        <span class="pull-right">{{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->format('D jS F \a\t G:i') }}} | {{{ $post->privacy }}} |&nbsp;</span>
    </div>
    <div class="panel-body">
        <p>{{{ $post->message }}}</p>
    </div>

    <ul class="list-group">
        @foreach ($comments as $comment)
            @if ($comment->post_id == $post->id)
                <li class="list-group-item">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#">
                        <img alt="avatar" width="20" height="20" src="{{{ User::find($post->user_id)->avatar->url() }}}"  class="img-circle">
                    </a>
                    {{{ User::find($comment->user_id)->fullName }}} said {{{ $comment->message }}}
                    <span class="pull-right">{{{ $comment->created_at }}}
                        @if (Auth::id() == $comment->user_id)
                        <a href="{{{ URL::to('comment='.$comment->id.'/delete') }}}">Delete</a>

                        @endif
                    </span>
                </li>
            @endif
        @endforeach

        <!-- New comment -->
        @if(Auth::check())
        <li class="list-group-item">
            {{ Form::open(array('route' => 'comment.store')) }}
            {{ Form::hidden('post_id', $post->id) }}
            {{ Form::textarea('message', '', array('placeholder'=>'Write a comment...','class' => 'form-control', 'rows' => '2')) }}
            {{ Form::submit('Post comment', array('class' => 'btn btn-sm btn-info')) }}
            {{ Form::close() }}
        </li>
        @endif

    </ul>
</div>