@extends('layouts.app')

@section('title')
    @if (Auth::check() && Auth::id() == $user->id)
        My
    @else
        {{{ $user->fullName }}}'s
    @endif
    Profile
@stop

@section('content')
    <div class="row">
        <!-- LEFT COLUMN -->
        <div class="col-md-4">
            <a href="#">
                <img alt="avatar" width="350" height="350" src="{{{ User::find($user->id)->avatar->url() }}}"  class="img-thumbnail">
            </a>

            <h1>{{{ $user->fullName }}}</h1>
            <h4>{{{ $user->DOB->diffInYears() }}} years old</h4>
            <br>
            @if(Auth::check() && Auth::id() == $user->id)
            {{ Form::model($user, array('route' => array('user.update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal', 'files' => 'true'))  }}
                Fullname: {{ Form::text('fullName', $user->fullName, ['placeholder'=>'Fullname','class' => 'form-control']) }}
                Birthday: {{ Form::text('DOB', $user->DOB->format('Y-m-d'), ['placeholder'=>'YYYY-MM-DD','class' => 'form-control']) }}
                Avatar: {{ Form::file('avatar') }}
            <br>
                {{ Form::submit('Update', array('class' => 'btn btn-sm btn-info btn-block')) }}
            {{ Form::close() }}
            @endif

            {{-- if not current user --}}
            @if (Auth::check() && Auth::id() != $user->id)

                @if ( Auth::user()->isFriend($user->id) || User::find($user->id)->isFriend(Auth::id()) )
                    {{-- Delete Friend Button --}}
                    <div class="user-button">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ URL::to('unfollow/'.$user->id) }}" class="btn btn-danger btn-lg btn-block">
                                    <i class="fa fa-envelope"></i> Unfollow
                                </a>
                            </div>
                        </div>
                    </div>
                @else
                    {{-- Add Friend Button --}}
                    <div class="user-button">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::open(array('route' => 'friendship.store')) }}
                                {{ Form::hidden('friend_id', $user->id); }}
                                <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-envelope"></i> Follow</button>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                @endif

            @endif
        </div>

        <!-- RIGHT COLUMN -->
        <div class="col-md-8">
            <div class="ibox float-e-margins">

            @if (Auth::check() && Auth::user()->id == $user->id)
                {{--@include('user._profile-edit')--}}
            @endif

            <!-- START ACTIVITY FEED -->
            @foreach($posts as $post)
                @if (Auth::check() && $post->user_id == Auth::id())
                    @include('posts.post_display')
                @elseif (Auth::check() && $post->privacy == 'Friends')
                    @if (Auth::user()->isFriend($post->user_id) || User::find($post->user_id)->isFriend(Auth::id()))
                        @include('posts.post_display')
                    @endif
                @elseif ($post->privacy == 'Public')
                    @include('posts.post_display')
                @endif
            @endforeach
            <!-- END ACTIVITY FEED -->
            </div>
        </div>
    </div>

@endsection