@extends('layouts.app')

@section('title')
    Search
@stop

@section('content')

    <div class="row">
        @foreach ($users as $user)

            <div class="panel panel-success">
                <div class="panel-heading">
                    <a href="{{{ URL::to('/user/'.$user->id) }}}">
                        <img alt="avatar" width="60" height="60" src="{{{ User::find($user->id)->avatar->url() }}}"  class="img-circle">
                        <h3 class="panel-title">{{{ $user->fullName }}}</h3>
                    </a>
                </div>
                <div class="panel-body">
                    {{{ $user->email }}} <a href="{{{ URL::to('/user/'.$user->id) }}}">View Profile</a>
                </div>
            </div>

        @endforeach
    </div>

@endsection