@extends('layouts.app')

@section('title')
    Posts
@stop

@section('content')

    {{ Form::open(array('action' => 'UserController@store', 'files' => 'true')) }}
        <div class="form-group">
            <label for="email">Email address</label>
            {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Email')) }}
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
        </div>
        <div class="form-group">
            <label for="fullName">Full name</label>
            {{ Form::text('fullName', '', array('class' => 'form-control', 'placeholder' => 'Full Name')) }}
        </div>
        <div class="form-group">
            <label for="DOB">Date of birth</label>
            {{ Form::text('DOB', '', array('class' => 'form-control', 'placeholder' => 'YYYY-MM-DD')) }}
        </div>
        <div class="form-group">
            <label for="avatar">Profile Image</label>
            {{ Form::file('avatar') }}
        </div>
        {{ Form::submit('Register', array('class' => 'btn btn-block btn-primary')) }}
    {{ Form::close() }}

@endsection