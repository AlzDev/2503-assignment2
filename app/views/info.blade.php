<!--THIS PAGE IS FOR MARKING PURPOSES. DISPLAYS THE SHORT DOCUMENT -->
@extends('layouts.app')

@section('title')
    Document
@stop
@section('post')ee
    Documentation
@stop

@section('content')
    <p>The project at hand was successfully completed to the extent of the requirements.<br>
    Nothing fancy was used, apart from maybe the way the count function was implemented to count all the comments relative to a post:
    
    <br>  $commentCount = DB::table('comments')->where('post_id', $postid)->count();
    
    Bootstrap was used to give the project a frame, but CSS took precedence when coming to the final touches.
    </p>
@stop
