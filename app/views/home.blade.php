@extends('layouts.app')

@section('title')
    Home
@stop

@section('content')

    @if (Auth::check())
        @include('posts.post_form')
    @endif

    @foreach($posts as $post)

        @if (Auth::check() && $post->user_id == Auth::id())
            <!-- Show post if current user is the author -->
            @include('posts.post_display')
        @elseif (Auth::check() && $post->privacy == 'Friends')
            @if (Auth::user()->isFriend($post->user_id) || User::find($post->user_id)->isFriend(Auth::id()))
                <!-- Show only if logged in and the owner of the post is a friend -->
                @include('posts.post_display')
            @endif
        @elseif ($post->privacy == 'Public')
            <!-- Show all public posts -->
            @include('posts.post_display')
        @endif

    @endforeach

@endsection