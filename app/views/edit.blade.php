@extends('layouts.app')

@section('title')
    Edit Post
@stop

@section('content')
    <!-- Update Status -->
    <div class="row">

        @include('posts.post_update')

    </div>

    <!-- Post -->
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="social-feed-separated">

                @include('posts.post_display')

            </div>
        </div>
    </div>
@stop