@extends('layouts.app')

@section('title')
    ER Diagram
@stop

@section('content')

    <div class="col-md-8">
        <object width="900" height="400" data="ERD.pdf"></object>
    </div>

@stop
