<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">AlanBook</a>
        </div>
        
        <div id="navbar" class="collapse navbar-collapse">
          
          
          <ul class="nav navbar-nav">

            @if (Auth::check())
            <li {{{ (Request::is('/') ? 'class=active' : '') }}}><a href="/">Home</a></li>
            <li {{{ (Request::is('user/'.Auth::user()->id) ? 'class=active' : '') }}}>
              <a href="{{ URL::route('user.show', array(Auth::user()->id)) }}">Profile</a>
            </li>
            <li><a href="{{ URL::route('friendship.show', array(Auth::user()->id)) }}">Friends</a></li>
            <li><a href="docs">Documentation</a></li>
            <li><a href="erd">ERD</a></li>
            @else
              <li {{{ (Request::is('/') ? 'class=active' : '') }}}><a href="/">Home</a></li>
              <li><a href="#contact">Users</a></li>
              <li><a href="#contact">Documentation</a></li>
              <li><a href="#contact">ERD</a></li>
            @endif
          </ul>
          
          
          @if (Auth::check())
        
            <form class='navbar-form navbar-right'>
              <span style='color:white;'>
                Welcome {{ Auth::user()->fullName }}
              </span>
              
              <a href="{{ URL::to('/logout') }}">
                  <i class="fa fa-sign-out"></i> Log out
              </a>
            </form>
          
          @else
            {{ Form::open(array('route' => 'user.login', 'class' => 'navbar-form navbar-right')) }}
            
            <div class="form-group">
              {{ Form::email('email', '', array('class' => 'form-control', 'placeholder' => 'Email', 'required' => '')) }}
            </div>
            <div class="form-group">
              {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'required' => '')) }}
            </div>
            {{ Form::submit('Sign in', array('class' => 'btn btn-success')) }}
            <a href="{{ URL::to('/register') }}" class="btn btn-danger">Create Account</a>
            
            {{ Form::close() }}
          @endif
          

        </div><!--/.nav-collapse -->
      </div>
</nav>